

        -----------
        SYMFONY 4.4
        -----------

        1/ INTRO

            - Framework :
                - squelette / Cadre de travail
                - Une modélisation déjà "optimisée"
                - Services (Composants, dependances)

            - Pourquoi utiliser un Framework

                - Framework maison : Pédagogie ++ / Communauté --
                - Mini Framework : Evolutif, léger et facile à déployer (Slim, Silex, Code Igniter... )
                - Framework Full Stack : Beaucoup de service (securite, routing, Formulaire, BDD etc..) / Communauté +++++




        2/ INSTALLATION DE SF 4.4

            a - Composer : Gestionnaire de dépendances.
                www.getcomposer.org
                Attention à bien installer composer en local (composer-setup.exe) et la bonne version de PHP utiliser par votre serveur local.

            b - Symfony CLI

                site de Symfony : Installation du CLI Symfony pour faire certaines de manière plus pratiques.


            c - Installation

                1- Composer :
                <cmd>Symfony/
                composer create-project symfony/website-skeleton Test "^4.4"


            d - Lancement de l'app

                1- composer
                <cmd>
                php -S localhost:8000 -t public


            e - Arborescence des dossiers et fichiers

                - bin : executables de symfony
                - config : Les config de notre app
                - public : Repertoire web/ (image, css, js, fonts, index.php)
                - src : Le code de votre app (php Controller, entity, model...)
                - templates : les vues de votre app (view)
                - var : tout ce qui est écrit par SF (Cache et les logs)
                - translations : traductions
                - tests : tests unitaires
                - vendor : Coeur de l'application
                .env : Les variables d'environnement (config, connexion à la BDD...)
                - composer.json : Liste des dépendances


    3/ Route et Controller

        A/ Création d'un controller
        <cmd>
        php bin/console make:controller

        B/ Les différentes routes
        - @Route("/bonjour")  == route avec affichage + erreur
        - @Route("/hello") == route avec affichage sans erreur (objet Response)
        - @Route("/ciao/{prenom}") == route avec response + donnée dynamique
        - @Route("/hola")  == route avec vue (render)
        - @Route("/halo/{prenom}") ==  route avec vue + donnée dynamique
        - @Route("/redirect") == route avec redirection
        - @Route("/redirect2") == route avec redirection + donnée dynamique
        - @Route("/redirect3") == route avec redirection externe
        - @Route("/message") == route avec message flash


        C/ L'objet Response
            <code>
            use Symfony\Component\HttpFoundation\Response;

            - Le protocol HTTP requiert une requete et une reponse. L'objet Response en est la réponse.

            - L'objet response peut gérer (par exemple) l'affichage de l'entête de la page

            - Render() c'est lié à l'objet response

        D/ L'objet Request
            <code>
            use Symfony\Component\HttpFoundation\Request;

            - L'objet request représente la partie Requete du protocle HTTP.

            - Il permet de gérer : session, post, get, cookies, server...

        E/ La redirection

            - La redirection est utile lorsqu'un utilisateur a effectué une action qui requiert que la page (ou une autre page) soit chargée

            - Pour cibler la page vers laquelle on redirige, on cible le name de la route (comme pour les liens)

            ===> A partir de maintenant toutes nos routes doivent avoir un name.

        F/ Les messages flash

            - Les messages en session (flash) sont utiles pour afficher des informations à l'utilisateur (client ou admin). Tout est automatisé avec SF et le controller.



        3.2/ Creation d'un blog (installation)

        0/ Installation de git
            https://git-scm.com/downloads

        A/ Installation :
            <cmd>Symfony/
            symfony new Blog --full --version=lts


        B/ Lancement de l'app :
            <cmd>Blog/
            symfony serve

            Pour utiliser un certificat SSL :
            <cmd>
            symfony.exe server:ca:install

            =====> https://localhost:8000



        C/ Création de nos controllers :

            UserController :
                /inscription
                /connexion
                /deconnexion
                /profil
                /modif_profil
                /suppr_profil
                /validation_email
                /regenerer_password

            PostController :
                / --> accueil  --> liste de tous les posts
                /post/{id}
                /category/{cat}
                /recherche


            AdminController :
                /admin/post/list
                /admin/post/add
                /admin/post/delete/{id}
                /admin/post/update/{id}
                /admin/user/list
                /admin/user/add
                /admin/user/delete/{id}
                /admin/user/update/{id}


            CommentController :

            BaseController :
                /quisommesnous
                /contact
                /partenaire


        <cmd>
        php bin/console make:controller PostController
        php bin/console make:controller CommentController
        php bin/console make:controller UserController
        php bin/console make:controller BaseController
        php bin/console make:controller AdminController



        4/ Entity et Mapping (Doctrine ORM)

            A/ Concept Entité

                Class POPO : Plain Old PHP Object.
                Les Entités servent de modèle de construction.

            B/ Doctrine ORM

                ORM : Object Relationnal Mapping

                Grâce à Doctrine, nous n'allons plus manipuler de SQL, et nous n'allons plus mettre les mains dans PHPMYADMIN.
                Pour que cela soit possible, il faut expliquer à Doctrine à quoi correspondent nos tables/entité ----> Mapping



            C/ Création de l'entité User

                -> Blog/src/Entity/User.php


                Avec les annotations on mappe notre entité user :

                exemple pour l'ID :
                /**
                * @ORM\Id
                * @ORM\Column(name="id", type="integer", nullable=false)
                * @ORM\GeneratedValue(strategy="AUTO")
                */


            D/ Générer les entités en ligne de commande

                <cmd>Blog
                php bin/console make:entity

                -> nommer l'entité
                -> créer chaque champs/propriété type/length/nullable


            E/ Création de la BDD à partir des entités

                1/ <code>.env
                    PC : DATABASE_URL=mysql://root:@127.0.0.1:3306/blog?serverVersion=5.7
                    MAC : DATABASE_URL=mysql://root:root@127.0.0.1:8889/blog?serverVersion=5.7

                2/ <cmd>
                    php bin/console doctrine:database:create
                    php bin/console make:migration
                    php bin/console doctrine:migrations:migrate


                    ==> Le systeme de migration de Doctrine, permet de consrver un historique des modifications du schema de la BDD (fonctionne sous le principe du versionning).



            ---
            F/ Créer les entités à partir de la BDD

             - On peut imaginer le cas inverse, où l'on récupère une base de données et l'on souhaite créer les entités correspondantes :

             <cmd>
             php bin/console doctrine:mapping:import "App\Entity" annotation --path=src/Entity

             /!\ les entités générées ne sont pas parfaite (getter/setter - lien avec le repository)

             @ORM\Entity(repositoryClass="App\Repository\CommentRepository")

             <cmd>
             php bin/console make:entity --regenerate
             App\Entity








        5/ TWIG et des Assets

         A/ Twig

            Twig est un moteur de template. L'objectif d'utiliser un moteur de template est de faciliter la présence du PHP dans les vues HTML.
            Il existe plusieurs moteurs de template : Blade, TPL, Smartie
            Twig est le moteur de template de SF, mais s'utilise également dans tous vos projets PHP.


         B/ Héritage Twig

            La logique de Twig, est de fonctionner sous forme de layout (structures de page). On peut définir plusieurs structures de page dans un projet (front vs back)

            Chaque vue va hériter d'un layout (base.html.twig - base2.html.twig - base_admin.html.twig), et va renseigner du contenu dans les block prévus à cet effet (content, js, css, slideshow, menu etc...)

            Dans un layout on peut déclarer autant de block que l'on souhaite et la vue peut renseigner du contenu à l'intérieur des block OU PAS !

            On peut déclarer du contenu par défaut dans un block, et si la vue souhaite remplacer ce contenu, il suffit de déclarer le block et d'y mettre du contenu.

            Cela nous offre une modularité complète !!!


         C/ Variables

            php : <?= $prenom ?>
            Twig : {{ prenom }}

            php : <?= $user['prenom'] ?>
            Twig : {{ user.prenom }}

            php : <?= $user -> getPrenom() ?>
            Twig : {{ user.prenom }}

            - Creation d'un variable Twig
            {% set prenom = 'Yakine' %}


         D/ Boucles Twig

            - le langage ne propose qu'un seul type de boucle : For (while, for, foreach)

            php : <?php foreach($tab as $item) : ?>
            twig : {% for item in tab %}

            php : <?php for($i=1; $i <= 10; $i++) : ?>
            twig : {% for i in 1..10 %}

            php : <?php for($i=10; $i >= 1; $i--) : ?>
            twig : {% for i in 10..1 %}

         E/ Conditions Twig

         F/ Lien et ressources


         G/ Documentation Twig

            https://twig.symfony.com/doc/3.x/






        6/ Fixtures

        - Les fixtures nous permettent d'avoir du "faux contenu" dans notre site, et ainsi de pouvoir travailler sur une version "non vide"

       - Pour se faire nous devons récupérer le service fixtures
       <cmd>
       composer require orm-fixtures

       - Ensuite nous créer nos objet dans la classe AppFixtures.php dans le dossier src/DataFixtures.

       Pour les mettres dans la BDD :
       <cmd>
       php bin/console doctrine:fixtures:load
       php bin/console doctrine:fixtures:load --append
       php bin/console doctrine:fixtures:load --purge-with-truncate

       ==> AppFixtures.php
       ==> Plusieurs fichiers de fixtures : post, user, comment


       tuto : https://blog.gary-houbre.fr/developpement/symfony/symfony-4-comment-mettre-en-place-des-fixtures


       -----------------------
       ASTUCES : Pour avoir de plus jolies fixtures, on peut utiliser le composant FZANINOTTO - FAKER...

       composer require fzaninotto/faker

       TUTO : https://github.com/fzaninotto/Faker
       --> cf notre fichier AppFixtures.php
       -----------------------



        7/ Doctrine dbal (Echange avec la BDD)

            A/ Le service Doctrine

            Doctrine DBAL : DataBase Abstracdt Layer. Doctrine DBAL, est une sur-couche de PDO.

            Avec Doctrine DBAL, vous n'allez plus gérer vous-même la connexion à la BDD, il va le faire lui-même.

            Et Doctrine DBAL, vous permet  : 1/ Exécuter des requête SQL génériques, 2/ créer vos propres requêtes, spécifiques à, votre projet.


            B/ Accéder au service Doctrine

            $repo = $this -> getDoctrine() -> getRepository(Post::class);
                -> findAll()
                -> find($id)
                -> findBy()
                -> findAllCategories() -> select distinct...

            $manager = $this -> getDoctrine() -> getManager();
                -> find(Post::class, 12)
                -> persist($post)
                -> remove($post)

            $repo = $manager -> getRepository(Post::class);


            On peut également injecter directement le manager dans nos fonctions (route)

                -> use Doctrine\Common\Persistence\ObjectManager;
                public function index(ObjectManager $manager){}

                Ou

                -> use Doctrine\ORM\EntityManagerInterface;
                public function index(EntityManagerInterface $manager){}

            On peut injecter directement le repository dans nos fonctions

                -> use App\Repository\PostRepository;
                public function index(PostRepository $repository){}


            C/ Requete SELECT * FROM ...

                    $repository -> findAll();


            D/ Requete SELECT * FROM ... WHERE id= ...

                    $repository -> find($id)
                    $manager -> find(Post::class, $id);

            E/ Requete SELECT * FROM ... WHERE .... = ... ORDER BY .... LIMIT

                    $respository -> findBy(array('category' => 'actualités'));

                    $repository -> findBy(array(
                        'category' => 'tshirt',
                        'color' => 'rouge',
                        'taille' => 'XL',
                        'marque' => 'Calvin Klein',
                        'prix' => '10.00'
                    ), array(
                        'prix' => 'DESC'
                    ), 10, 0);


                    $respository -> findOneBy(array('pseudo' => 'ben'));

            Lien : https://symfony.com/doc/current/doctrine.html


            F/ Requete insert/update

             - Avec SF (doctrine), que l'on enregistre une nouvelle entrée dans la BDD, ou qu'on modifie une entrée existante, cela fonctionne de la même manière. La différence étant que quand on insère, on part d'un objet vide, alors que quand on modifie on part d'un objet existant.

             $manager -> persist($post);
             $manager -> flush();

             ==> On peut modifier l'objet après qu'il soit persisté et avant qu'il soit flush();
             ==> On peut persister plusieurs objects, et ils seront enregistrer ensemble dans la BDD, au moment du flush.



            G/ Requete Delete

            - La particularité de la suppression d'une entrée avec SF (Doctrine), est que avant de supprimer l'entrée, il faut récupérer l'objet.

            $manager -> find(Post::class, $id)
            $manager -> remove($post);
            $manager -> flush();



            H/ Create query (DQL) et query builder (PHP)

            - Doctrine DBAL nous offre plusieurs manière d'effectuer des requêtes génériques... mais parfois notre application, nécessite ses propres requêtes spécifiques... Dans ce cas il existe deux outils pour les coder :

                QueryBuilder ===> PHP
                CreateQuery ====> SQL (DQL)

            CF PostRepository.php

            /!\ Idéalement, il faut coder ses requête spécifique dans les répository. Cela permet de factoriser notre code.



        8/ Formulaire et Validation

            A/ Fonctionnement des formulaires
            B/ Générer un formulaire
            C/ Récupérer les données d'un formulaire
            D/ Affichage d'un formulaire
            E/ Personnaliser un formulaire avec bootstrap
            F/ La validation des données
            G/ Champs file (upload d'une image par exemple)
            H/ Lien doc sur les formulaires 


        9/ Sécurité et Utilisateur
        10/ Association Mapping
        11/ Swiftmailer
        12/ WebPack Encore
        13/ Mise en prod
        14/ AJAX sur SF
        15/ Gestion des pages d'erreurs
        16/ Probleme N+1
        17/ les slugs
