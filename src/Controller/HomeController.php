<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Produit;
use App\Repository\ProduitRepository;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     * 
     */
    public function index()
    {
        // Récupérer la liste de tous les produits
        $repository = $this->getDoctrine()->getRepository(Produit::class);
        $produit = $repository->findAll();

        return $this->render('home/index.html.twig', [
            'produit' => $produit
        ]);
    }
    /**
     * @Route("/recherche", name="recherche")
     * 
     */
    public function recherche(Request $request,ProduitRepository $pr)
    {
        $motRecherche = $request->query->get('recherche');
        if($motRecherche){
            $liste_produits = $pr->findByTitreCategorieDescription($motRecherche);
        }else{
            $liste_produits = [];
        }
        return $this->render('home/index.html.twig', [
            'produit' => $liste_produits,
            'mot_recherche' => $motRecherche
        ]);
    }
}
