<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use App\Entity\Membre;

use function PHPSTORM_META\elementType;

class MembreFixtures extends BaseFixture
{
    public function loadData(ObjectManager $manager)
    {
        //La methode createMany est défini dans BaseFixture.
        //la fonction qui est passée en 31ème paramètre, renvoi l'entité créée 
        //cette fonction va être executée autant de fois que vaut le 1er paramètre
        $membre = new Membre;
        $membre->setEmail("admin@boutique.fr");
        $membre->setPassword(password_hash("admin", PASSWORD_DEFAULT));
        $membre->setNom("Min");
        $membre->setPrenom("Ad");
        $membre->setAdresse("rue Quelque Part");
        $membre->setCodePostal("75000");
        $membre->setVille("Paris");
        $membre->setRoles(["ROLE_ADMIN", "ROLE_MODERATEUR"]);
        $membre->setCivilite("h");
        $membre->setPseudo("Admin");
        $manager->persist($membre);

        
        $this->createMany(10,'membre',function($num){

            $membre = new Membre;
            $membre->setEmail("membre" . $num ."@yopmail.com");
            $membre->setPseudo("membre" . $num);
            $membre->setPassword(password_hash("membre" . $num ,PASSWORD_DEFAULT));
            $membre->setNom($this->faker->lastName);
            $membre->setPrenom($this->faker->firstName);
            $membre->setAdresse($this->faker->address);
            $membre->setCodePostal(substr($this->faker->postcode,0,5));
            $membre->setVille(substr($this->faker->city,0,20));
            $membre->setCivilite($this->faker->randomElement(['h','f','a']));
            $membre->setRoles(['ROLES_USER']);
            
            return $membre;
        });
        // $product = new Product();
        // $manager->persist($product);
       
        $manager->flush();
    }
}
