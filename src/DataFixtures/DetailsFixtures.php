<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Details;
use
Doctrine\Common\DataFixtures\DependentFixtureInterface as DFI;

class DetailsFixtures extends BaseFixture implements DFI
{
    public function loadData(ObjectManager $manager)
    {
        
        $this->createMany('40','detail',function($num){
            $detail_commande = new Details;
            $detail_commande->setCommande($this->getRandomReference("commande"));
            $produit = $this->getRandomReference("produit");
            $detail_commande->setProduit($produit);
            $quantite = $this->faker->randomNumber(2);
            $detail_commande->setQuantite($quantite);
            $detail_commande->setPrix($quantite * $produit->getPrix());
            return $detail_commande;
        });
        $manager->flush();
    }
    public function getDependencies(){
        // J'indique les fixtures qui doivent être lancées avant la fixture actuelle.
        return [ 
            CommandeFixtures::class, ProduitFixtures::class 
        ];

    }
}
