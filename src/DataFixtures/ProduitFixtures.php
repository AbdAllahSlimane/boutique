<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Produit;


class ProduitFixtures extends BaseFixture
{
    public function loadData(ObjectManager $manager)
    {
        $this->createMany(50, "produit", function ($num) {
            $produit = new Produit;
            $produit->setReference($this->faker->text(10));
            $produit->setTitre('produit' . $num);
            $categorie = $this->faker->randomElement(['pull', 'jean', 'pullOver', 'jean-mom']);
            $produit->setCategorie($categorie);
            $produit->setCouleur($this->faker->rgbcolor());
            $produit->setTaille($this->faker->randomElement(['M', 'L', 'S']));
            $produit->setPublic($this->faker->randomElement(['f', 'h', 'mixte']));
            $produit->setPhoto($categorie . '.jpg');
            $produit->setStock($this->faker->randomFloat(1, 3, 5));
            $produit->setPrix($this->faker->randomNumber(5));
            $produit->setDescription($this->faker->text(150));
            return $produit;
        });

        $manager->flush();
    }
}
